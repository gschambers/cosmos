var path = require("path");
var webpack = require("webpack");
var env = process.env.NODE_ENV || "development";

function isProduction() {
    return env === "production";
}

var plugins = [
    new webpack.NormalModuleReplacementPlugin(
        /^(net|dns)$/,
        path.resolve(__dirname, 'src/ui/util/shim.js')
    ),
    new webpack.DefinePlugin({
        "process.env": {
            NODE_ENV: `"${env}"`,
            API_ROOT: `"${process.env.API_ROOT}"`,
            API_STREAM_ROOT: `"${process.env.API_STREAM_ROOT}"`
        }
    })
];

if (isProduction()) {
    plugins.push(
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin()
    );
}

module.exports = {
    entry: {
        index: "./src/ui/index.js",
        // test: "./src/ui/test.js"
    },

    output: {
        filename: "[name].js",
        path: path.resolve("public")
    },

    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel" }
        ]
    },

    plugins: plugins,

    devtool: isProduction() ? "source-map" : "eval"
};
