import actionTypes from "../constants/actionTypes";

export default function(state, action) {
    switch (action.type) {
        case actionTypes.SET_ACTIVE_PLAYER:
            return action.data;
    }

    return state;
}
