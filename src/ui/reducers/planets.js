import actionTypes from "../constants/actionTypes";

export default function(state, action) {
    let planet;

    switch (action.type) {
        case actionTypes.ADD_PLANET:
        case actionTypes.UPDATE_PLANET:
            planet = action.data;
            state[planet.id] = planet;
            break;

        case actionTypes.REMOVE_PLANET:
            planet = action.data;
            delete state[planet.id];
            break;
    }

    return state;
}
