import activePlayerReducer from "./activePlayer";
import planetsReducer from "./planets";
import {combineReducers} from "../util/store";

export default combineReducers({
    activePlayer: activePlayerReducer,
    planets: planetsReducer,
});
