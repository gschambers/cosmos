import joi from "joi";

export function oneOf(enumMap) {
    return joi.any().only(
        Object.keys(enumMap).map((key) => enumMap[key])
    )
}
