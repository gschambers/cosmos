import {Subject} from "rxjs/Rx";

export function createDispatcher() {
    const dispatcher = new Subject();

    return {
        dispatch(action) {
            dispatcher.next(action);
        },

        observe() {
            return dispatcher.asObservable();
        }
    };

}

export function createStore(reducer, initialState, dispatcher) {
    return dispatcher.observe()
        .scan(reducer, initialState)
        .startWith(initialState)
        .publishReplay(1)
        .refCount();
}

export function combineReducers(reducers) {
    const keys = Object.keys(reducers);

    return function(state, action) {
        const nextState = Object.assign({}, state);
        return keys.reduce((state, key) => {
            const reducer = reducers[key];
            state[key] = reducer(state[key], action);
            return state;
        }, nextState);
    };
}
