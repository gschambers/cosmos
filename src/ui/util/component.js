import React, {Component} from "react";

export function withSelector(source$) {
    return function(Child) {
        class WrappedComponent extends Component {
            componentWillMount() {
                this.subscription = source$
                    .subscribe((state) => this.setState(state));
            }

            componentWillUnmount() {
                this.subscription.unsubscribe();
            }

            render() {
                return React.createElement(Child, {
                    ...this.props,
                    ...this.state
                });
            }
        }

        WrappedComponent.displayName = `Connect(${Child.displayName || Child.name})`;

        return WrappedComponent;
    };
}
