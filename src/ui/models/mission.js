import joi from "joi";
import {CoordsSchema} from "./shared/coords";

const MissionSchema = joi.object({
    id: joi.number().required(),
    origin: CoordsSchema.required(),
    target: CoordsSchema.required()
});

export function createMission(props) {
    return MissionSchema.attempt(props);
}
