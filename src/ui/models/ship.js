import joi from "joi";
import {oneOf} from "../util/schema";

export const ShipTypes = {
    CARGO: "cargo",
    DESTROYER: "destroyer",
    FIGHTER: "fighter",
    PASSENGER: "passenger",
    SCOUT: "scout",
    STATION: "station"
};

export const ShipSchema = joi.object({
    id: joi.string().required(),
    name: joi.string().required(),
    type: oneOf(ShipTypes).required(),
    range: joi.number().min(1).max(10).default(5).required(),
    maxCrew: joi.number().min(0).max(25).default(3).required(),
    crew: joi.array().items(joi.number()).required()
});

export function createShip(props) {
    return ShipSchema.attempt(props);
}
