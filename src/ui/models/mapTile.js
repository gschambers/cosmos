import joi from "joi";
import {CoordsSchema} from "./shared/coords";

const MapTileSchema = joi.object({
    coords: CoordsSchema.required(),
    planet: joi.number()
});

export function createMapTile(props) {
    return MapTileSchema.attempt(props);
}
