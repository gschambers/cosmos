import joi from "joi";
import {oneOf} from "../util/schema";
import {AttributesSchema} from "./shared/attributes";

export const CharacterRoles = {
    CLERIC: "cleric",
    CIVILIAN: "civilian",
    ENGINEER: "engineer",
    MEDIC: "medic",
    PILOT: "pilot",
    SOLDIER: "soldier",
    THIEF: "thief",
};

export const CharacterTemperaments = {
    FRIENDLY: "friendly",
    HOSTILE: "hostile",
    NEUTRAL: "neutral",
};

export const CharacterSchema = joi.object({
    name: joi.string().required(),
    role: oneOf(CharacterRoles).required(),
    temperament: oneOf(CharacterTemperaments).required(),
    attributes: AttributesSchema.required(),
});

export function createCharacter(props) {
    return CharacterSchema.attempt(props);
}
