import joi from "joi";
import {oneOf} from "../util/schema";

export const PlanetTypes = {
    TERRESTRIAL: "terrestrial",
    OCEAN: "ocean",
    GAS: "gas"
};

export const PlanetSchema = joi.object({
    id: joi.number().required(),
    name: joi.string().required(),
    system: joi.number().required(),
    type: oneOf(PlanetTypes).required()
});

export function createPlanet(props) {
    return PlanetSchema.attempt(props);
}
