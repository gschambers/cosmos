import joi from "joi";

export const CoordsSchema = joi.object({
    x: joi.number().required(),
    y: joi.number().required()
});
