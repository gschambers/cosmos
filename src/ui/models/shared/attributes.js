import joi from "joi";

export const Attributes = {
    BRAVERY: "bravery",
    CHARISMA: "charisma",
    FAITH: "faith",
    INTELLECT: "intellect",
    LOYALTY: "loyalty",
    LUCK: "luck",
    SPEED: "speed",
    STRENGTH: "strength",
};

export const AttributesSchema =
    joi.object(
        Object.keys(Attributes)
            .reduce((attributes, key) => ({
                [key]: joi.number().min(0).max(10).default(0).required(),
                ...attributes
            }), {})
    );
