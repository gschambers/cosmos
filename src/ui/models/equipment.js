import joi from "joi";
import {oneOf} from "../util/schema";
import {AttributesSchema} from "./shared/attributes";

export const EquipmentTypes = {
    ARMOR: "armor",
    ITEM: "item",
    WEAPON: "weapon",
};

const EquipmentSchema = joi.object({
    type: oneOf(EquipmentTypes).required(),
    attributes: AttributesSchema.required()
});

export function createEquipment(props) {
    return EquipmentSchema.attempt(props);
}
