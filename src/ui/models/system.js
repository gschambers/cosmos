import joi from "joi";

export const SystemSchema = joi.object({
    id: joi.string().required(),
    name: joi.string().required()
});

export function createSystem(props) {
    return SystemSchema.attempt(props);
}
