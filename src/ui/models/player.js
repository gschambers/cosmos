import joi from "joi";

const PlayerSchema = joi.object({
    id: joi.number().required(),
    name: joi.string().required(),
    character: joi.number().required()
});

export function createPlayer(props) {
    return joi.attempt(props, PlayerSchema);
}
