import THREE from "three";
import {loadTexture} from "./texture";

const BASE_RADIUS = 0.5;

const defaultOptions = {
    size: 1,
    bumpMap: false,
    specularMap: false,
    cloudMap: false,
};

export async function createPlanet(options) {
    const mesh = new THREE.Object3D();
    const renderHooks = [];
    const textures = await loadPlanetTextures(options);

    const planetMaterialOptions = {
        map: textures.shift(),
    };

    if (getOption(options, "bumpMap")) {
        Object.assign(planetMaterialOptions, {
            bumpMap: textures.shift(),
            bumpScale: 0.05,
        });
    }

    if (getOption(options, "specularMap")) {
        Object.assign(planetMaterialOptions, {
            specularMap: textures.shift(),
            specular: new THREE.Color("grey"),
            shininess: 10,
        });
    }

    const radius = getOption(options, "size") * BASE_RADIUS;

    const planet = new THREE.Mesh(
        new THREE.SphereGeometry(radius, 32, 32),
        new THREE.MeshPhongMaterial(
            planetMaterialOptions
        )
    );

    mesh.add(planet);

    // TODO: Configurable rotation
    renderHooks.push(
        (delta) => planet.rotateY((1 / 32) * delta)
    );

    if (getOption(options, "cloudMap")) {
        const clouds = new THREE.Mesh(
            new THREE.SphereGeometry(radius + 0.01, 32, 32),
            new THREE.MeshPhongMaterial({
                map: textures.shift(),
                side: THREE.DoubleSide,
                opacity: 0.8,
                transparent: true,
                depthWrite: false
            })
        );

        mesh.add(clouds);

        // TODO: Configurable wind speed (cloud rotation)
        renderHooks.push(
            (delta) => clouds.rotateY((1 / 16) * delta)
        );
    }

    return {
        mesh,
        renderHooks
    };
}

function loadPlanetTextures(options) {
    const name = getOption(options, "name");

    const textureRequests = [
        loadTexture(`textures/planets/${name}/map.png`)
    ];

    if (getOption(options, "bumpMap")) {
        textureRequests.push(
            loadTexture(`textures/planets/${name}/bump.png`)
        );
    }

    if (getOption(options, "specularMap")) {
        textureRequests.push(
            loadTexture(`textures/planets/${name}/spec.png`)
        );
    }

    if (getOption(options, "cloudMap")) {
        textureRequests.push(
            loadTexture(`textures/planets/${name}/cloud.png`)
        );
    }

    return Promise.all(textureRequests);
}

function getOption(options, key) {
    return key in options ?
        options[key] :
        defaultOptions[key];
}
