import THREE from "three";
import noop from "../util/noop";

const loader = new THREE.TextureLoader();

export function loadTexture(url) {
    return new Promise((resolve, reject) => {
        loader.load(url, resolve, noop, reject);
    });
}
