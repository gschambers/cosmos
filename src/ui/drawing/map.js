import THREE from "three";

const MAP_TILE_SIZE = 0.75;
const X_OFFSET = MAP_TILE_SIZE * 1.5;
const Y_OFFSET = MAP_TILE_SIZE * (Math.sqrt(3) / 2);

const material = new THREE.MeshPhongMaterial();

export function createMap(size=1) {
    const tiles = [];
    const renderHooks = [];

    const coords = [];

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            coords.push([i, j]);

            if (i > 0) {
                coords.push([-1 * i, j]);
            }

            if (j > 0) {
                coords.push([i, -1 * j]);
            }

            if (i > 0 && j > 0) {
                coords.push([-1 * i, -1 * j]);
            }
        }
    }

    while (coords.length) {
        const [x, z] = coords.shift();
        const y = -x - z;
        tiles.push(createMapTile(x, y));
    }

    const mesh = merge(tiles);

    mesh.rotateX(-0.75);
    mesh.translateY(-0.25);
    mesh.translateZ(-0.75);

    return {
        mesh,
        renderHooks
    };
}

function merge(tiles) {
    const geometry = new THREE.Geometry();

    for (const tile of tiles) {
        geometry.merge(tile.geometry, tile.matrix);
    }

    return new THREE.Mesh(geometry, material);
}

function createMapTile(x, y) {
    const geometry = new THREE.Geometry();

    geometry.vertices.push(
        createVertex(x, y, 0),
        createVertex(x, y, 1),
        createVertex(x, y, 2),
        createVertex(x, y, 3),
        createVertex(x, y, 4),
        createVertex(x, y, 5),
        createVertex(x, y, 0)
    );

    geometry.faces.push(
        new THREE.Face3()
    );

    return new THREE.Mesh(
        geometry,
        material
    );
}

function createVertex(x, y, n) {
    const deg = (60 * n) + 30;
    const rad = (Math.PI / 180) * deg;

    return new THREE.Vector3(
        x + (MAP_TILE_SIZE * Math.sin(rad)),
        y + (MAP_TILE_SIZE * Math.cos(rad)),
        1
    );
}
