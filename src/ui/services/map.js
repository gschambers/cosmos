import axios from "axios";
import {createMapTile} from "../models/mapTile";

// Get matrix of map tiles of `size` around coords (`x`, `y`)
export function getMapTilesNearCoords(x, y, size=3) {
    return axios.get(`/api/map?x=${x}&y=${y}&size=${size}`)
        .then((res) => res.data.map((row) => row.map(createMapTile)));
}
