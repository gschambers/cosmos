import axios from "axios";
import {createEquipment, EquipmentTypes} from "../models/equipment";

export function getEquipmentById(equipmentId) {
    return axios.get(`/api/equipment/${equipmentId}`)
        .then((res) => res.data.map(createEquipment));
}

export function getArmor() {
    return getEquipmentByType(EquipmentTypes.ARMOR);
}

export function getItems() {
    return getEquipmentByType(EquipmentTypes.ITEM);
}

export function getWeapons() {
    return getEquipmentByType(EquipmentTypes.WEAPON);
}

function getEquipmentByType(type) {
    return axios.get(`/api/equipment?type=${type}`)
        .then((res) => res.data.map(createEquipment));
}
