import http from "../util/http";
import {createPlayer} from "../models/player";

export function getActivePlayer() {
    return http.get(`/api/players/me`)
        .then((res) => createPlayer(res.data));
}

export function getPlayerById(playerId) {
    return http.get(`/api/players/${playerId}`)
        .then((res) => createPlayer(res.data));
}
