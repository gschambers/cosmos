import http from "../util/http";
import {createPlanet} from "../models/planet";

export function getPlanetsBySystem(systemId) {
    return http.get(`/api/systems/${systemId}/planets`)
        .then((res) => res.data.map(createPlanet));
}
