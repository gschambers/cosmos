import axios from "axios";
import {createMission} from "../models/mission";

export function getMissionById(missionId) {
    return axios.get(`/api/missions/${missionId}`)
        .then((res) => res.data.map(createMission));
}

export function getRandomMissionsByPlanet(planetId, count=1) {
    return axios.get(`/api/planets/${planetId}/missions?count=${count}`)
        .then((res) => res.data.map(createMission));
}

export function getActiveMissionsByPlayer(playerId) {
    return getMissionsByPlayer(playerId, false);
}

export function getCompletedMissionsByPlayer(playerId) {
    return getMissionsByPlayer(playerId, true);
}

function getMissionsByPlayer(playerId, completed) {
    return axios.get(`/api/players/${playerId}/missions?completed=${completed}`)
        .then((res) => res.data.map(createMission));
}
