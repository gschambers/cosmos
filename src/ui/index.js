import "babel-polyfill";

import domready from "domready";
import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, browserHistory} from "react-router";

import CosmosApp from "./components/CosmosApp";
import Game from "./components/Game";

function main() {
    ReactDOM.render(
        <Router history={browserHistory}>
            <Route path="/" component={CosmosApp}>
                <IndexRoute component={Game} />
            </Route>
        </Router>,
        document.querySelector("#app")
    );
}

domready(main);
