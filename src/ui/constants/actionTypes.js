import keymirror from "keymirror";

export default keymirror({
    FETCH_ACTIVE_PLAYER: null,
    SET_ACTIVE_PLAYER: null,
    SET_PLANET: null,
});
