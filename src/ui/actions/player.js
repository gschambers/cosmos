import axios from "axios";
import * as playersService from "../services/players";
import actionTypes from "../constants/actionTypes";
import {dispatcher} from "../store";

export async function fetchActivePlayer() {
    dispatcher.dispatch({
        type: actionTypes.FETCH_ACTIVE_PLAYER,
        data: {
            state: "pending"
        }
    });

    try {
        const player = await playersService.getActivePlayer();

        dispatcher.dispatch({
            type: actionTypes.SET_ACTIVE_PLAYER,
            data: player
        });

        dispatcher.dispatch({
            type: actionTypes.FETCH_ACTIVE_PLAYER,
            data: {
                state: "success"
            }
        })
    } catch (error) {
        dispatcher.dispatch({
            type: actionTypes.FETCH_ACTIVE_PLAYER,
            data: {
                state: "error",
                error
            }
        });
    }
}
