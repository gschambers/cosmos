import "babel-polyfill";
import domready from "domready";
import THREE from "three";
import {createMap} from "./drawing/map";
import {createPlanet} from "./drawing/planet";

const VIEW_ANGLE = 45;
const NEAR = 0.1;
const FAR = 1000;

const renderer = new THREE.WebGLRenderer({ alpha: true });
renderer.setClearColor(0xffffff, 0);
const scene = new THREE.Scene();

let camera;

const globalRenderHooks = [];

async function main() {
    const elem = document.querySelector("#app");

    const width = window.innerWidth - 20;
    const height = window.innerHeight - 20;

    renderer.setSize(width, height);

    camera = createCameraWithAspect(width / height);
    camera.position.set(0, 0, 8);
    camera.lookAt(scene.position);

    let dir = [0, 0];

    window.addEventListener("keydown", (evt) => {
        switch (evt.key) {
            case "ArrowUp":
                dir[1] = -1;
                break;

            case "ArrowDown":
                dir[1] = 1;
                break;

            case "ArrowLeft":
                dir[0] = -1;
                break;

            case "ArrowRight":
                dir[0] = 1;
                break;
        }
    });

    window.addEventListener("keyup", () => {
        dir[0] = 0;
        dir[1] = 0;
    });

    globalRenderHooks.push(
        (delta) => {
            camera.position.set(
                camera.position.x + (dir[0] * 5 * delta),
                camera.position.y + (-1 * dir[1] * 5 * delta),
                camera.position.z + (dir[1] * 5 * delta)
            )
        }
    )

    scene.add(camera);

    const planets = [
        {
            name: "earth",
            size: 1,
            bumpMap: true,
            specularMap: true,
            cloudMap: true,
            position: [0, 0, 0],
        },
    ];

    const {mesh} = createMap(2);
    scene.add(mesh);

    for (const planetOptions of planets) {
        const {mesh, renderHooks} = await createPlanet(planetOptions);

        scene.add(mesh);

        mesh.position.set(
            ...planetOptions.position
        );

        globalRenderHooks.push(
            ...renderHooks
        );
    }

    const directionalLight = new THREE.DirectionalLight(0xcccccc, 1);
    directionalLight.position.set(5, 5, 5);
    scene.add(directionalLight);

    const ambientLight = new THREE.AmbientLight(0x888888);
    scene.add(ambientLight);

    elem.appendChild(renderer.domElement);

    requestAnimationFrame(render);
}

function createCameraWithAspect(aspect) {
    return new THREE.PerspectiveCamera(VIEW_ANGLE, aspect, NEAR, FAR);
}

let lastMS = null;

function render(nowMS) {
    requestAnimationFrame(render);

    if (!lastMS) {
        lastMS = nowMS - (1000 / 60);
    }

    const delta = Math.min(200, nowMS - lastMS);

    lastMS = nowMS;

    globalRenderHooks.forEach(
        (fn) => fn(delta / 1000, nowMS / 1000)
    );

    renderer.render(scene, camera);
}

function tryCatch(fn) {
    return async function(...args) {
        try {
            return await fn(...args);
        } catch (err) {
            throw err;
        }
    };
}

domready(tryCatch(main));
