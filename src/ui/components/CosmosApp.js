import React, {PropTypes} from "react";

export default function CosmosApp({children}) {
    return (
        <div className="cosmos-app">
            {children}
        </div>
    );
}

CosmosApp.propTypes = {

};
