import React, {Component, PropTypes} from "react";
import THREE from "three";
import {Observable} from "rxjs/Rx";
import {fetchActivePlayer} from "../actions/player";
import {withSelector} from "../util/component";
import {store} from "../store";

const cameraPosition = new THREE.Vector3(0, 0, 5);
const lightPosition = new THREE.Vector3(0, 5, 5);

class Game extends Component {
    static defaultProps = {
        activePlayer: null
    };

    componentWillMount() {
        fetchActivePlayer();
    }

    render() {
        const {activePlayer} = this.props;

        console.log(activePlayer);

        return (
            <div className="game">

            </div>
        );
    }
}

export default withSelector(
    store
        .distinctUntilKeyChanged("activePlayer")
        .map((state) => ({
            activePlayer: state.activePlayer
        }))
)(Game);
