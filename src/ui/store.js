import {createDispatcher, createStore} from "./util/store";
import rootReducer from "./reducers";

export const dispatcher = createDispatcher();

dispatcher.observe().subscribe(
    (action) => console.log(action)
);

const initialState = {
    activePlayer: null,
    characters: {},
    equipment: {},
    mapTiles: {},
    missions: {},
    planets: {},
    players: {},
    ships: {},
    systems: {},
};

export const store = createStore(
    rootReducer,
    initialState,
    dispatcher
);
