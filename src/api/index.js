import cors from "cors";
import express from "express";

const app = express();

app.use(cors());

app.get("/api/players/me", (req, res) => {
    res.json({
        id: 1,
        name: "Gary Chambers",
        character: 1,
    });
});

export default app;
